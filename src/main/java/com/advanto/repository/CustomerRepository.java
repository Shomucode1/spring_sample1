package com.advanto.repository;

import java.util.List;

import com.advanto.model.Customer;

public interface CustomerRepository {

	List<Customer> findAll();

}
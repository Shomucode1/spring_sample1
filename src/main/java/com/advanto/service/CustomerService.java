package com.advanto.service;

import java.util.List;

import com.advanto.model.Customer;

public interface CustomerService {

	List<Customer> findAll();

}
package com.advanto.service;

import java.util.List;

import com.advanto.model.Customer;
import com.advanto.repository.CustomerRepository;
import com.advanto.repository.HibernateCustomerRepositoryImpl;

public class CustomerServiceImpl implements CustomerService {

	private CustomerRepository customerRepository = new HibernateCustomerRepositoryImpl();
	
	
	@Override
	public List<Customer> findAll() {
		return customerRepository.findAll();
	}
}

